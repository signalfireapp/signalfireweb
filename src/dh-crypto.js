

var assert = require("assert");
var crypto = require("crypto");

// 512 can be replaced with a higher number (2048) however it takes significantly longer to compute
//The prime must be shared with all users wishing to join
var server = crypto.createDiffieHellman(2048);
var prime = server.getPrime();




//Generate alice's keys
var alice = crypto.createDiffieHellman(prime);
alice.generateKeys();
var alicePub = alice.getPublicKey();



//Generate Bob's keys
var bob = crypto.createDiffieHellman(prime);
bob.generateKeys();
var bobPub = bob.getPublicKey();

// compute the secret using the shared prime
var bobAliceSecret = bob.computeSecret(alicePub);
var aliceBobSecret = alice.computeSecret(bobPub);


console.log('\nAlice Public:\n', alicePub.toString('base64'));
console.log('\nBob Public:\n', bobPub.toString('base64'));
console.log('\nBob & Alice Secret:\n', bobAliceSecret.toString('base64'));
console.log('\nAlice & Bob Secret: \n', aliceBobSecret.toString('base64'));






// public keys are different, but secret is common
assert.notEqual(bobPub.toString('base64'), alicePub.toString('base64'));
assert.equal(bobAliceSecret.toString('base64'), aliceBobSecret.toString('base64'));
// use common secret as shared encryption key





//ENCRYPTING
//encrypting using bobAliceSecret

var algorithm = 'aes256';
var message = 'Super secret string! woooooooooo!!';
console.log('\n\nMessage to encrypt: ', message);
var cipher = crypto.createCipher(algorithm, bobAliceSecret.toString('hex'));
var encrypted = cipher.update(message, 'utf8', 'hex') + cipher.final('hex');
console.log('Encrypted message: ', encrypted);

//DECRYPTING
//decrypting using aliceBobSecret

var decipher = crypto.createDecipher(algorithm, aliceBobSecret.toString('hex'));
var decrypted = decipher.update(encrypted, 'hex', 'utf8') + decipher.final('utf8');
console.log('Decrypted message: ', decrypted);
assert.equal(message, decrypted);

/*

 // shared secret with 3rd person
 var carol = crypto.createDiffieHellman(prime);
 carol.generateKeys();
 var carolPub = carol.getPublicKey();

 var carolAliceSecret = carol.computeSecret(alicePub);
 var aliceCarolSecret = alice.computeSecret(carolPub);

 assert.notEqual(carolPub, alicePub);
 assert.equal(carolAliceSecret, aliceCarolSecret);

 // secret depends on pairs
 assert.notEqual(aliceBobSecret, aliceCarolSecret);
 var carolBobSecret = carol.computeSecret(bobPub);
 assert.notEqual(carolAliceSecret, carolBobSecret);

 */