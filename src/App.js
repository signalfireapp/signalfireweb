import React from 'react';
import './App.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

/*
class App extends Component {
  constructor(props){
	super(props)
	
  }

  render() {
    return (
    <MuiThemeProvider>
      <div className="container-fluid">
      </div>
    </MuiThemeProvider>
    );
  }
}

export default App;
*/

export default function App({children}) {
	return (
	<MuiThemeProvider>
		<div className="container-fluid">
			{children}
		</div>
		</MuiThemeProvider>
	)
}
