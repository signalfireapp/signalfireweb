// Add an export for your reducers here.
// e.g: export componentfilename from './componentfilename'
//

// Remember to exclude the ".js" from componentfilename.
/*
import {combineReducers} from 'redux'
import * as reducers from './connectionStatus'

export default combineReducers({
	...reducers
})
*/


export { default as connectionStatus} from './connectionStatus'
export { default as xmppInstance} from './xmppUserStorage'
export { default as contacts} from './contactList'
