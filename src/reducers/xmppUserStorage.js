import XMPP from 'stanza.io'


const initialState=new XMPP.createClient()

export default function xmppUserStorage(state=initialState,action){
	switch(action.type){
		case 'STORE_USER':
			return action.xmppInstance
		default:
			return state
	}
}
