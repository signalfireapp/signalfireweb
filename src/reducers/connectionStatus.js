
import {CONNECT,DISCONNECT,BADCREDENTIALS} from '../constants'

const initialState = {
	isConnected:false,
	reason: 'none',
	connectMessage:'Not Connected'
}

const updateConnect = (state = initialState, action) => {
	if(action.type === CONNECT && state.isConnected !== true){
		return { 
			isConnected : true , 
			reason:initialState.reason,
			connectMessage: state.connectMessage || initialState.connectMessage};
	}
	else if(action.type === BADCREDENTIALS){
		return Object.assign({},state,{
			isConnected : false,
			reason:'bad_credentials',
		connectMessage: action.connectMessage 
		});
	}
	else if(action.type === DISCONNECT && state.isConnected !== false){
		return { isConnected : false ,
			reason:initialState.reason,
		connectMessage: state.connectMessage || initialState.connectMessage
		};
	}
	return state

}

export default updateConnect
