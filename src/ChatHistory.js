import React, {Component} from 'react';
import {Row} from 'react-materialize';
import {List, ListItem} from 'material-ui/List';

class ChatHistory extends Component {

    render() {
        var textLeft = {
            textAlign: "left"
        }
        var testMessage = "This is a string passed to the list ListItem"
        return (
            <Row>
                <List style={{
                    backgroundColor: 'White',
                    height: 630,
                    overflow: 'auto'
                }}>
                    <ListItem
                        primaryText={"MESSAGE: " + testMessage}
                        disabled={true}
                        style={textLeft}
                    />
                </List>
            </Row>
        )
    }
}

export default ChatHistory;
