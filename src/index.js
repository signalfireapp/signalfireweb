import React from 'react';

import { createDevTools } from 'redux-devtools'
import LogMonitor from 'redux-devtools-log-monitor'
import DockMonitor from 'redux-devtools-dock-monitor'

import thunkMiddleware from 'redux-thunk'

import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {browserHistory,IndexRoute, Route,Router} from 'react-router';
import Login from './Login';
import Interface from './interface';
import NotFound from './NotFound';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import {routerMiddleware,syncHistoryWithStore,routerReducer} from 'react-router-redux'

import * as reducers from './reducers'
//import updateConnection from './reducers/connectionStatus'

const reducer = combineReducers({
	...reducers,
//	updateConnection,
	routing:routerReducer
})

const DevTools = createDevTools(
		  <DockMonitor toggleVisibilityKey="ctrl-h" changePositionKey="ctrl-q">
		      <LogMonitor theme="tomorrow" preserveScrollTop={false} />
		        </DockMonitor>
		)

const rMiddleware = routerMiddleware(browserHistory);


const store = createStore(
		reducer,
		DevTools.instrument(),
		applyMiddleware(thunkMiddleware, rMiddleware)
)
console.log(store.getState());

const history = syncHistoryWithStore(browserHistory, store)


ReactDOM.render(
	(
	<Provider store={store}>
	<div>
	<Router history={history}>
	<Route path="/" component={App} >
		<IndexRoute component={Interface} />
		<Route path="login" component={Login} />
		<Route path="*" component={NotFound} />
	</Route>
	</Router>
	<DevTools />
	</div>
	</Provider>
	),
  document.getElementById('root')
)
