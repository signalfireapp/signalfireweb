import React, {Component} from 'react';
import {Col} from 'react-materialize';
import ChatHistory from './ChatHistory';
import MessageBox from './MessageBox';

class ChatBox extends Component {
    render() {
        return (
            <Col s={8} m={10} style={{
                backgroundColor: 'Black',
                padding: 10,
                minHeight: 770,
                position: 'relative'
            }}>
                <div className="container-fluid">
                    <ChatHistory />
                    <MessageBox />
                </div>
            </Col>
        )
    }
}

export default ChatBox
