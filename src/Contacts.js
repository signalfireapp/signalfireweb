import React, {Component, PropTypes} from 'react';
import {Col} from 'react-materialize';
import {List, ListItem} from 'material-ui/List'
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import CommunicationChatBubble from 'material-ui/svg-icons/communication/chat-bubble';

class Contacts extends Component {
    constructor(props) {
        super(props);
        this.mSize = parseInt(props.m, 0) || 2;
        this.sSize = parseInt(props.s, 0) || 4;
    }

    contactItem(contact) {
        if (contact.subscription !== "none") {
            return (
                <ListItem key={contact.jid.full} primaryText={contact.jid.local}
                          rightIcon={<CommunicationChatBubble />}/>
            )
        }

    }

    render() {
        const {store} = this.context
        const state = store.getState()
        return (
            <Col s={this.sSize} m={this.mSize} style={{backgroundColor: 'Black', padding: 10}}>
                <List style={{backgroundColor: 'White', minHeight: 750}}>
                    <Subheader>Contacts</Subheader>
                    <Divider />
                    { state.contacts.map(this.contactItem) }
                </List>
            </Col>
        );
    }
}

Contacts.contextTypes = {
    store: PropTypes.object
}

export default Contacts;
