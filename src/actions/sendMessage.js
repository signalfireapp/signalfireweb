import XMPP from 'stanza.io';
import {MESSAGE_FAILED_TO_DELIVER, MESSAGE_SENT} from '../constants'

export const sendMessage = (client, username, message, key = '') => {


    return function (dispatch) {

        var xmpp = client;


	    xmpp.on('messageDelivered',function(status){
	    	dispatch({type:MESSAGE_FAILED_TO_DELIVER});
	    })

        xmpp.sendMessage({
                to: username,
                body: message
            }
        )


    }
};

