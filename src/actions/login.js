import XMPP from 'stanza.io';
import {GET_CONTACTS,CONNECT,BADCREDENTIALS, STORE_USER} from '../constants'
var JXT = require('jxt').createRegistry();

JXT.use(require('jxt-xmpp-types'));
JXT.use(require('jxt-xmpp'));


function connectSuccess(){
	return {type : CONNECT}
}

function connectFailure(){
	return {type : BADCREDENTIALS,connectMessage:"Incorrect username or password"}
}

export const connect_chat = (username,password='') => {


	return function(dispatch){
	console.log("Triggered the event")
        //    var user = new XMPP.JID('testuserlfirechat');
        var xmpp= new XMPP.createClient({jid:username,password:password,transport:'websocket',wsURL:"ws://signalfire.chat/websocket/"});

        xmpp.on('raw:incoming',function(xml){
            var stanza = JXT.parse(xml);
            var test = stanza.xml.children;
            if(test.length >0){
                if(test[0].name === 'host-unknown'){
		return dispatch(connectFailure());
                }
            }
        });

        xmpp.on('auth:failed', function(){
		return dispatch(connectFailure());
	}	
        );

        xmpp.on('session:started',function(){
//		dispatch(updateConnection(CONNECT));
		xmpp.getRoster().then((result)=>{
            		xmpp.sendPresence()
		if('items' in result.roster){
		dispatch({type:GET_CONTACTS, contactRoster:result.roster.items})
		}
		dispatch({type:STORE_USER, xmppInstance:xmpp})
		return  dispatch(connectSuccess());
		})
	 });

	console.log("Attempting connect");
		xmpp.enableKeepAlive();
        xmpp.connect()
	}

}

