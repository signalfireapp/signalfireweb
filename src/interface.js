import React, {PropTypes, Component} from 'react';
import Contacts from './Contacts';
import ChatBox from './ChatBox';
import {Row} from 'react-materialize';
import {push} from 'react-router-redux';

class Interface extends Component {

    force_login() {
        const {store} = this.context
        const state = store.getState();
        if (!state.connectionStatus.isConnected) {
            store.dispatch(push('/login'))
        }
    }

    componentDidMount() {
        const {store} = this.context;
        const state = store.getState();
        if (!state.connectionStatus.isConnected) {
            this.unsubscribe = store.subscribe(() => this.forceUpdate());
        }
    }

    render() {
        this.force_login();
        return (
            <Row className="contact-window">
                <Contacts m={2} s={4} clientSession={this.props.clientSession}/>

                <ChatBox />
            </Row>
        );
    }
}
Interface.contextTypes = {
    store: PropTypes.object
};


export default Interface;
