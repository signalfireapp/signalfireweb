import React,{PropTypes, Component} from 'react';
import { Button, Card, Row, Col, Input } from 'react-materialize';
import Alert from 'react-s-alert';
import {push} from 'react-router-redux'
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import { connect_chat } from './actions/login'


class Login extends Component{

    constructor(props) {
        super(props);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.state = {
            email:'',
            password:'',
	    visibleError:false
        };
	
    }

    handlePasswordChange(val){
        this.setState({password:val.target.value});
    }

    handleEmailChange(val){

        this.setState({email:val.target.value});
    }

    bad_login(message){
        Alert.warning(message,{
            position: 'top-right',
            effect: 'scale',
            beep: false,
            offset: 0
        });

    }
   
    componentDidMount() {
	    const {store} = this.context;
	    this.unsubscribe = store.subscribe(()=> this.forceUpdate()
			    );
    }

    componentWillUnmount(){
	    this.unsubscribe()
    }

    componentDidUpdate(){
	    const {store} = this.context
	    const state = store.getState();
	    if(state.connectionStatus.reason === "bad_credentials" && !this.state.visibleError){
	    	this.bad_login(state.connectionStatus.connectMessage);
		this.setState({ visibleError : true })
	    }
    
    }

    render() {
	    const {store} = this.context
	    const state = store.getState();

	    if(state.connectionStatus.isConnected){
		    store.dispatch(push('/'))
	    }
        return (
			<Row>
				<Alert stack={{limit:3}} timeout={5000} />
				<Col s={12} m={6} offset='m3'>
					<Card>
						<h1>SignalFire</h1>
						<form method="POST">
							<Input label="Username" name="username" s={12} onChange={this.handleEmailChange}/>
							<Input label="Password" type="password" s={12}  name="password" onChange={this.handlePasswordChange}/>
							<Button onClick={(event)=>{event.preventDefault();store.dispatch(connect_chat(this.state.email,this.state.password))}}>Login</Button>
						</form>
					</Card>
				</Col></Row>
        );

    }
}
Login.contextTypes ={
	store: PropTypes.object
};


export default Login;
