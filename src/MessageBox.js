import React, {Component, PropTypes} from 'react';
import {Row} from 'react-materialize';
import {TextField} from 'material-ui';
import {sendMessage} from './actions/sendMessage';

class MessageBox extends Component {
    constructor(props) {
        super(props);
        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            value: ''
        }
    }

    handleChange = (event) => {
        this.setState({
            value: event.target.value,
        })
    }

    handleKeyPress = (event) => {
        const {store} = this.context;
        const state = store.getState();
        if (event.key === 'Enter') {
            event.preventDefault()
            store.dispatch(sendMessage(state.xmppInstance, 'testuser@signalfire.chat', this.state.value))
            this.setState({
                value: ''
            })
        }
    }

    render() {
        const {store} = this.context;
        //const state = store.getState();
        return (
            <Row>
                <div style={{
                    position: 'absolute',
                    bottom: 5,
                    left: 10,
                    right: 10
                }}
                >
                    <TextField
                        floatingLabelText="Enter Message Here"
                        value={this.state.value}
                        multiLine={true}
                        rows={2}
                        fullWidth={true}
                        underlineShow={false}
                        onChange={this.handleChange}
                        onKeyDown={this.handleKeyPress}
                        style={{
                            backgroundColor: 'White',
                            maxHeight: 100,
                            minHeight: 100,
                            overflow: 'auto'
                        }}
                    />
                </div>
            </Row>
        )
    }
}

MessageBox.contextTypes = {
    store: PropTypes.object
};

export default MessageBox;
